import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccueilComponent } from './accueil/accueil.component';
import {MatButtonModule} from '@angular/material';



@NgModule({
  declarations: [AccueilComponent],
  imports: [
    CommonModule,
    MatButtonModule
  ]
})
export class AccueilModule { }
