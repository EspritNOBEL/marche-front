import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {MarcheModule} from './marche/marche.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ExposantModule} from './exposant/exposant.module';
import {InformationsModule} from './informations/informations.module';
import {AccueilModule} from './accueil/accueil.module';
import {MatIconModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AccueilModule,
    MarcheModule,
    ExposantModule,
    InformationsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
