import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MarcheComponent} from './marche/marche/marche.component';
import {ExposantComponent} from './exposant/exposant/exposant.component';
import {InformationsComponent} from './informations/informations/informations.component';
import {AccueilComponent} from './accueil/accueil/accueil.component';

const routes: Routes = [
  {
    path: 'marche',
    component: MarcheComponent
  },
  {
    path: 'exposant',
    component: ExposantComponent
  },
  {
    path: 'informations',
    component: InformationsComponent
  },
  {
    path: '**',
    component: AccueilComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
