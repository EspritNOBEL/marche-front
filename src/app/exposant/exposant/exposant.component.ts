import {Component, OnInit} from '@angular/core';
import {Exposant} from './exposant';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MarcheDialogComponent} from '../../marche/dialog/marche-dialog/marche-dialog.component';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ExposantService} from '../exposant-service';

@Component({
  selector: 'app-exposant',
  templateUrl: './exposant.component.html',
  styleUrls: ['./exposant.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ]),
  ]
})
export class ExposantComponent implements OnInit {

  isLoading = true;
  listeFindAllExposant: Exposant[];
  columnsToDisplay = ['nomCommercialExposant', 'nomResponsableExposant'];
  expandedElement: Exposant | null;

  constructor(
    private exposantService: ExposantService,
    public dialog: MatDialog,
    private matSnackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.findAllExposant();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MarcheDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.isCancelled !== true) {
        this.findAllExposant();
      }
    });
  }

  findAllExposant() {
    this.exposantService.findAllExposant().subscribe((result) => {
      this.listeFindAllExposant = result;
      this.isLoading = false;
    });
  }

}
