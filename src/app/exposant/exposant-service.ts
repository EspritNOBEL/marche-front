import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Exposant} from './exposant/exposant';

@Injectable({
  providedIn: 'root',
})
export class ExposantService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  findAllExposant(): Observable<Exposant[]> {
    return this.httpClient.get<Exposant[]>(environment.apiUrl + '/exposant/findAll');
  }

  createExposant(exposant: Exposant): Observable<Exposant> {
    return this.httpClient.post<Exposant>(environment.apiUrl + '/exposant/create', Exposant);
  }

  deleteExposant(id: number): Observable<Exposant> {
    return this.httpClient.delete<Exposant>(environment.apiUrl + `/exposant/deleteById/${id}`);
  }

}
