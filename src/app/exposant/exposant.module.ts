import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExposantComponent} from './exposant/exposant.component';
import {MatButtonModule, MatIconModule, MatProgressSpinnerModule, MatTableModule} from '@angular/material';
import {MarcheModule} from '../marche/marche.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [ExposantComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MarcheModule
  ]
})
export class ExposantModule {
}
