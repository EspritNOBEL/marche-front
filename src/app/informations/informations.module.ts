import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformationsComponent } from './informations/informations.component';



@NgModule({
  declarations: [InformationsComponent],
  imports: [
    CommonModule
  ]
})
export class InformationsModule { }
