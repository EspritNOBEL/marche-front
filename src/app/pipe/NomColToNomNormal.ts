import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'NomColToNomNormal'})
export class NomColToNomNormal implements PipeTransform {
  transform(input: string): string {
    switch (input) {
      case 'nomMarche': {
        return 'Marché';
      }
      case 'lieuMarche': {
        return 'Lieu';
      }
      case 'jourMarche': {
        return 'Jour';
      }
      case 'heureDebutMarche': {
        return 'Début';
      }
      case 'heureFinMarche': {
        return 'Fin';
      }

      case 'nomCommercialExposant': {
        return 'Nom commercial';
      }
      case 'nomResponsableExposant': {
        return 'Responsable';
      }

      default: {
        return 'XXXXX';
      }
    }
  }
}
