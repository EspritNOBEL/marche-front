export class Marche {

  id: number;
  nomMarche: string;
  lieuMarche: string;
  jourMarche: string;
  heureDebutMarche: string;
  heureFinMarche: string;

  nbStandsMaxMarche: number;
  nbStandsActuelMarche: number;
  prixMetreMarche: string;
  placierMarche: string;
  communeMarche: string;

}
