import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  isCancelled = true;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>
  ) {
  }

  ngOnInit() {
  }

  onCloseClick(): void {
    this.dialogRef.close();
    this.isCancelled = true;
  }

  onConfirm(): void {
    this.dialogRef.close();
    this.isCancelled = false;
  }

}
