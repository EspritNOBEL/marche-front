import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcheDialogComponent } from './marche-dialog.component';

describe('MarcheDialogComponent', () => {
  let component: MarcheDialogComponent;
  let fixture: ComponentFixture<MarcheDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcheDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcheDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
