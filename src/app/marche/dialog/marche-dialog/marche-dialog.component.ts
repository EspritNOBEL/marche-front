import {Component, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {Marche} from '../../marche';
import {MarcheService} from '../../marche-service';
import {Inject} from '@angular/core';

@Component({
  selector: 'app-marche-dialog',
  templateUrl: './marche-dialog.component.html',
  styleUrls: ['./marche-dialog.component.scss']
})
export class MarcheDialogComponent implements OnInit {

  marche: Marche;
  isCancelled = false;
  partieAdministrative = false;

  constructor(
    public dialogRef: MatDialogRef<MarcheDialogComponent>,
    private marcheService: MarcheService,
    private matSnackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit() {
    if (this.data.edition) {
      this.marche = this.data.marche;
    } else {
      this.marche = new Marche();
    }
  }

  onCloseClick(): void {
    this.dialogRef.close();
    this.isCancelled = true;
  }

  submit(edition: boolean) {
    if (!edition) {
      this.marcheService.createMarche(this.marche)
        .subscribe((result) => {
            this.matSnackBar.open('Le marché a été ajouté', 'OK', {
              duration: 4000,
              verticalPosition: 'top',
              panelClass: ['success-snackbar']
            });
          },
          error => this.matSnackBar.open('Un problème est survenu lors de l\'ajout du marché', 'OK',
            {
              duration: 4000,
              verticalPosition: 'top',
              panelClass: ['error-snackbar']
            })
        );
      this.dialogRef.close();
    } else {
      this.marcheService.updateMarche(this.marche)
        .subscribe((result) => {
            this.matSnackBar.open('Le marché a été modifié', 'OK', {
              duration: 4000,
              verticalPosition: 'top',
              panelClass: ['success-snackbar']
            });
          },
          error => this.matSnackBar.open('Un problème est survenu lors de la modification du marché', 'OK',
            {
              duration: 4000,
              verticalPosition: 'top',
              panelClass: ['error-snackbar']
            })
        );
      this.dialogRef.close();
    }


  }


}
