import {Component, OnInit} from '@angular/core';
import {Marche} from '../marche';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog, MatSnackBar} from '@angular/material';
import {MarcheDialogComponent} from '../dialog/marche-dialog/marche-dialog.component';
import {MarcheService} from '../marche-service';
import {ConfirmDialogComponent} from '../dialog/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-marche',
  templateUrl: './marche.component.html',
  styleUrls: ['./marche.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ]),
  ]
})
export class MarcheComponent implements OnInit {

  isLoading = true;
  listeFindAllMarche: Marche[];
  columnsToDisplay = ['nomMarche', 'lieuMarche', 'jourMarche', 'heureDebutMarche', 'heureFinMarche'];
  expandedElement: Marche | null;

  constructor(
    private marcheService: MarcheService,
    public dialog: MatDialog,
    private matSnackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.findAllMarche();
  }

  openDialog(modif, market): void {
    const dialogRef = this.dialog.open(MarcheDialogComponent, {
      width: '500px',
      data: {
        edition: modif,
        marche: market
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.isCancelled !== true) {
        this.findAllMarche();
      }
    });
  }

  findAllMarche() {
    this.marcheService.findAllMarche().subscribe((result) => {
      this.listeFindAllMarche = result;
      this.isLoading = false;
    });
  }

  openConfirmDialog(id: number): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.isCancelled !== true) {
        this.deleteMarche(id);
      }
    });
  }

  deleteMarche(id: number) {
    this.marcheService.deleteMarche(id).subscribe((result) => {
        this.findAllMarche();
        this.matSnackBar.open('Le marché a été supprimé', 'OK', {
          duration: 4000,
          verticalPosition: 'top',
          panelClass: ['success-snackbar']
        });
      },
      error => this.matSnackBar.open('Un problème est survenu lors de la suppression du marché', 'OK',
        {
          duration: 4000,
          verticalPosition: 'top',
          panelClass: ['error-snackbar']
        })
    );
  }

}
