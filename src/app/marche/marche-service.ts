import {Observable} from 'rxjs';
import {Marche} from './marche';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MarcheService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  findAllMarche(): Observable<Marche[]> {
    return this.httpClient.get<Marche[]>(environment.apiUrl + '/marche/findAll');
  }

  createMarche(marche: Marche): Observable<Marche> {
    return this.httpClient.post<Marche>(environment.apiUrl + '/marche/create', marche);
  }

  deleteMarche(id: number): Observable<Marche> {
    return this.httpClient.delete<Marche>(environment.apiUrl + `/marche/deleteById/${id}`);
  }

  updateMarche(marche: Marche): Observable<Marche> {
    return this.httpClient.put<Marche>(environment.apiUrl + '/marche/update', marche);
  }

}
