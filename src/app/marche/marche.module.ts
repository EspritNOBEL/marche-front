import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MarcheComponent} from './marche/marche.component';
import {MatTableModule} from '@angular/material/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NomColToNomNormal} from '../pipe/NomColToNomNormal';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import { MarcheDialogComponent } from './dialog/marche-dialog/marche-dialog.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import { ConfirmDialogComponent } from './dialog/confirm-dialog/confirm-dialog.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  declarations: [
    MarcheComponent,
    NomColToNomNormal,
    MarcheDialogComponent,
    ConfirmDialogComponent
  ],
  exports: [
    NomColToNomNormal
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatInputModule,
    FormsModule,
    MatSlideToggleModule
  ],
  entryComponents: [
    MarcheDialogComponent,
    ConfirmDialogComponent
  ]
})
export class MarcheModule {
}
